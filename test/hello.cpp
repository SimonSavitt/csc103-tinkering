/* hello.cpp
 *
 * This is the standard "hello world" program, which you will see just about
 * every time you learn a new language (except for maybe haskell...).
 * To compile and run, just execute "g++ hello.cpp" followed by "./a.out".
 */

#include <iostream>
using std::cout;

int main() {
	cout << "Hello, and welcome to csc103.\n";
	return 0;
}

// vim:foldlevel=4
