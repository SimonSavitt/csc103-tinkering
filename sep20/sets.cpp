// working with sets and maps.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <set>
using std::set;
#include <string>
using std::string;

// example 1: let's just get some strings from standard input
// and store them in a set.
/* NOTE: parameter T needs to be by reference so that
 * we can change it! */
void readStrings(set<string>& T)
{
	// read strings, and store them in a set.
	string s;
	while (cin >> s) {
		T.insert(s);
	}
}
/* NOTE: to "fake" a by value call with a large
 * parameter (e.g., a giant set), you can pass the
 * parameter *const* by reference, like this: */
//void readStrings(const set<string>& T);
/* The const modifier makes sure that the function
 * will not change the contents of the set. */


/* example 2: prime test with memory. */

/* first, re-implement your primality test as a function.
 * Then, give your function "memory" so that it uses sets
 * to remember the answers to prior calls.  I.e., before
 * it actually does a computation, it will check its database
 * of known answers.
 * */

// these are at global scope, so they will retain
// their contents between calls to primeTestWithMemory.
set<unsigned long> primes;
set<unsigned long> nonprimes;

// TODO: modify this function (or start over from scratch)
// so that it remembers the answers as described above.
bool primeTestWithMemory(unsigned long n)
{
	// we want a set for the primes, but if we
	// declare it here, its life will be too short.
	// set<unsigned long> primes;
   if(n==0||n==1) return false;
   else if(n==2) return true; //only even prime
   else if(n%2!=0) 
   {
	   for(unsigned long i=3; i<(n-2); i++)
	   {
		   if(n%i==0) return false;
	   }
   }
   else
	   return false;
   return true;
   // Thanks to Tenzin for sharing his answer!
}

int main(void)
{
	// NOTE: set is *not a datatype*.  Like the vector, it is a
	// machine for making datatypes.
	set<string> S;
	readStrings(S);
	for (set<string>::iterator i = S.begin(); i != S.end(); i++) {
		// okay, so...  how does the iterator help?
		//cout << S[i] << endl; // no.  T.T
		// we have to use the "dereference operator".  This will
		// make more sense when we learn pointers.  For now, just
		// accept it...
		cout << *i << endl;
	}

	// TODO: fill out this main function to test
	// the primetest function above.
	// TODO: write more code to test out the modified prime test.
	// make sure you ask repeated questions...

	return 0;
}
