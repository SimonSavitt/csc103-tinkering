/* Flow of control:
 * Boolean expressions; if and while statements.
 * */

// first, import our usual libraries:
#include <iostream>
using std::cin;  // <-- new stuff!  To avoid typing the std:: prefix all the
				 // time, you can instad make a "using" statement.  It says
				 // that unless otherwise specified, the symbol "cin" refers
				 // to "std::cin".
using std::cout;
using std::endl;
#include <string>
using std::string;

int main()
{
	// first, we'll try an if statement.
	cout << "How are you liking 103? ";
	string response;
	getline(cin,response);
	if (response == "yes") {  // NOTE: two equals signs!
		cout << "Yay!  Me too 8D\n";
	}
	else {
		cout << "That's too bad...  T.T\n";
	}
	// Here is the general format, which you can find in your book:
	/* if (<boolean expression>) {
	       <zero or more statements (of any kind!)>
	   }
	*/
	// quick example of what goes wrong with confusing = and ==:
	int x;
	cin >> x;
	if ((x = 7)) {
		cout << "Hahahahaha\n\n";
		// this will ALWAYS happen!
		// Why?  Well, "x = 7" actually has a value  (which is 7)
		// Important fact: you can use any integer expression as a boolean,
		// with the following convention:
		// 0 <==> false and anything else <==> true
	}

	// More flow of control:  looping
	// print obnoxious message exactly 18 times:
	int i = 0; // counter variable for the loop
	while (i < 18)
	{
		cout << "Hahahahaha\n\n";
		i = i + 1; // NOTE: there is shorthand for this: ++i;
	}
	cout << "i == " << i << "\n\n"; // this will print 18.  Make sure you know
	                                // why
	// TODO: delete the braces from the above while loop and see what happens.

	// let's try to print a list of the first n perfect squares,
	// i.e, 1,4,9,...,n^2, where n is from the user.
	// Code is courtesy of Dan.  Thanks!
	int n = 0;
	i = 1;
	cout << "give me n: \n";
	cin >> n;
	while (i <= n) {
		// cout << i*i + "\n";
		// This first attempt looks plausible, and it actually compiles,
		// yet it does NOT do what we want.  you must consider the
		// *datatypes* of the expressions on either side of the + sign.
		// The result, believe it or not, will be a memory address @_@
		// I don't expect you to understand all of this now, we'll come back
		// to it later on, but for now:
		// TODO: uncomment the version with the plus sign, and see what
		// happens.
		// Here is what we really want:
		cout << i*i << "\n"; // NOTE: we can combine multiple <<'s together.
		i++;
	}
	
	/* More about datatypes */
	/* datatypes tell the compiler not only the size, but also other
	 * useful meta information about your variables: */
	// TODO: make sure you know the basic datatypes!:
	// int, char, long, double, bool,...
	char c = 'A'; // NOTE: character constants are denoted by
				  // single quotes, not double!
	short s = 'A'; // same as 65
	cout << "c == " << c << "\ts == " << s << "\n\n";
	return 0;
	




}

// vim:foldlevel=2
